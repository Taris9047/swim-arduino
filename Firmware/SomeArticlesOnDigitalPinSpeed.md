# Some Articles on Arduino's Digital Pin Speed.

1. https://timodenk.com/blog/port-manipulation-and-arduino-digitalwrite-performance/

    Claims 'fast' function can write as fast as 1.75 us timeframe while default ```digitalWrite``` shows 5 us.

    ```c
    void digitalWriteFast(uint8_t pin, uint8_t x) {
      if (pin / 8) { // pin >= 8
        PORTB ^= (-x ^ PORTB) & (1 << (pin % 8));
      }
      else {
        PORTD ^= (-x ^ PORTD) & (1 << (pin % 8));
      }
    }
    ```

1. https://www.best-microcontroller-projects.com/arduino-digitalwrite.html
    Claims default ```digitalWrite``` can be sped up 20 times.