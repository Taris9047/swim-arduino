#!/usr/bin/env -m python
#
# Simple LED blinking example for CircuitPython
# Device: Arduino Nano RP2040 Connect
#
# Written by Taylor Shin
# Sep 22th 2022
#
import board
import time
from digitalio import DigitalInOut, Direction

# Ports --> Adjust this code to board.D10 for external LED blinking
LED = DigitalInOut(board.LED)
# Setting up the LED pin direction: Obviously, output.
LED.direction = Direction.OUTPUT

# Blink Data
data = [0, 0, 1, 0, 0, 1, 1]

# Delay in seconds
delay_on = 0.333
delay_off = 0.250

# Starting the main loop
print(
    "Now bliking Arduino Nano RP2040 Connect's Digital port {} with "
    "{}(ON), {}(OFF) sec of interval".format(LED, delay_on, delay_off)
)

data_len = len(data)  # Length of the data
index = 0  # Spans from 0 to data_len-1

print("CircuitPython Blinker for Arduino Nano RP2040 Connect")
while True:

    if data[index] != 0:
        LED.value = True
        print("Data: {} LED On".format(data[index]))
        time.sleep(delay_on)
    else:
        LED.value = False
        print("Data: {} LED Off".format(data[index]))
        time.sleep(delay_off)

    if index >= data_len-1:
        index = 0
    else:
        index += 1

