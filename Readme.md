# SWIM Arduino

## Arduino Sketches
1. In ```Sketches``` directory, we have a little bit of collection of Arduino Sketch codes. Arduino ships with firmware that can run those Sketches which are quite simple to understand compared to other microcontroller's firmwares. So, these sketches are literally 'firmware' part of Arduino microcontrollers, so to say. But remember, just call them Sketches not actual firmwares.
1. By 'firmware' we define basic behavior of Arduino. You can define LED blinking behavior directly into the firmware as depicted in the ```BlinkTest``` code. But you can also program arduino to accept outside control via serial connection. The section below, ```BlinkPy``` is based on this 'external control' scheme.
1. Of course external control scheme is somewhat limited since each the control step depend on serial communication's baud rate. Thus we cannot maximize the speed up to the limit (90.466 kHz) with external control.

## Arduino Python Control (BlinkPy)
1. To Run this script correctly, you need to set up Arduino with ```Standard Firmata``` from Arduino's Sketchbook examples. We'll adjust this code to fit our purpose later. But for COM port control, this is fine. The same code can be found in the ```Sketches``` directory.
1. The default LED bliking port is D13, Arduino's term: Digital Port 13. So, connect LED accordingly. Circuit diagram is shown below.

    ![Diode Connection Schematic](./BlinkPy/ArduinoLEDCOnnection.png)

1. Note that D13 port is shared with Arduino UNO board's status LED. So, it will blink shortly when you start communication with the board.
1. Make sure LED is serial connected with at least 120 Ohm resistor. If the resistor is not present, you might fry the port due to excessive current.
1. 1 kOhm is usually fine. The calculation is, you get 5 V output, which becomes around 4.3 V due to LED's built-in (forward) bias (it is usually 0.6-7 V) with 220 Ohm, we get 22 mA of current flowing through the LED. This is usually fine but make sure the current do not exceed 40 mA.
1. If resistor is too high, i.e. 100 kOhm, etc., you'll find LED too dim. This is one of parameter we need to figure out with the SiPM test as well.
1. To run the python code, you need to install ```pyfirmata``` from PyPI repository. Just use ```pip install -U pyfirmata``` to install it. If you do not have system access right, make sure you create a virtualenv first.
1. Using Python virtualenv is as easy as,
    ```shell
    python -m venv ./.venv
    ./.venv/bin/activate
    ```
    or, in Windows

    ```powershell
    python.exe -m venv ./.venv
    .\.venv\Scripts\activate
    ```

    Once running those command, you'll find ```.venv``` directory generated in the BlinkPy directory. Then you can initiate the ```pip``` command to install the ```pyfirmata``` package into the ```.venv``` directory. 
    
    If you would like to quit current virtual environment, simply just close the terminal window or use ```./.venv/bin/deactivate``` command.
	
## RP2040 Connection with [MP001252](https://canada.newark.com/multicomp-pro/mp001252/led-blue-650mcd-473nm-1411/dp/41AH2085)

Connecting the blue LED to Arduino is a bit different than the general blue LED as depicted above. According to the [datasheet of MP001252](https://www.farnell.com/datasheets/2868668.pdf), the diode supports up to 20 mA of forward current under bias between 2.8 V to 3.6 V and 0.1 V of forward voltage drop down. Considering we are using Arduino RP2040 connect's GPIO, 3.3 V logic, we can calculate the resistor for the maximum current: 160 Ohm of series connection. 

Usually there is another component we need to worry about. The current drivablity of the controller itself. According to [RP2040's datasheet page 239](https://datasheets.raspberrypi.com/rp2040/rp2040-datasheet.pdf), the maximum drive strength can be up to 12 mA. However, this is just current sourcing, not the maximum current it can withstand. Since each GPIO is terminated with 40 Ohm of resistors, it can theoritically withstand up to 80 mA. Thus, having 20 mA for Diode connection will not hurt.

![Pinout of RP2040 Connect](./BlinkPy/ABX00053-pinout-LED.png)

As you can see here, the RP2040 connect has much more flexibilty in terms of pinout settings. For now, we will be just using `one` pin to drive the LED. Recommendation is D2 to D10 since they do not have too much of functionality other than digital I/O. You can use GND pin to make a circuit with the LED. Say, D10 connects to anode of the diode, cathode of diode is connected to a resistor, then the other end of the resistor connects to GND. Other than that, programming RP2040 Connect will be pretty straightforward.

On the other hand, if you really like to use Python instead of Arduino C, you can visit [this site](https://learn.adafruit.com/circuitpython-on-the-arduino-nano-rp2040-connect/install-circuitpython) to use Circuit Python programming for RP2040 Connect. Circuit Python is developed by Adafruit Industries and provides a lot of additional libraries for pheripherals driving. 

## CircuitPython Code for RP2040 Connect
[BlinkCircuitPy](./BlinkCircuitPy) has an example code for blinking LED with the RP2040 Connect. Make sure you have already baked the Adafruit CircuitPython bootloader into RP2040 Connect. The guide can be found [here](https://learn.adafruit.com/circuitpython-on-the-arduino-nano-rp2040-connect/install-circuitpython). If double tapping the reset button, you need to manually reset by shorting the reset pin into GND while pressing the reset button. The pin locations can be found [here](https://docs.arduino.cc/static/5c5af556c300faec4a8c01af07ba5784/a6d36/SHORT-REC-NANORP2040CONNECT.png). The figure above also highlights the reset/GND pins with red rectangle. To short those two terminals, using a jumper cable is recommended but you can just tab those pins with screwdriver or anything electrically conductive enough. 
