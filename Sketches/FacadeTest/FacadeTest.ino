/* 
 *  Simple LED Facade written by Taylor Shin
 *  July 20th 2022
 *  
 *  Adopted from https://bcourses.berkeley.edu/courses/1464145/assignments/7819009
 */

// Wait time for facades
#define WAIT 25

// LED Port (Digital Port 12, D12, for now. But you can define 1-13 anytime. 
// Using PWM availe pin 9 (3,5,6,9,10,11)
#define LED_PORT 9

unsigned int i = 0; // loop counter
int DEBUG = 1; 
byte led_val = 255; // Led brightness, brightest at first.

// the setup function runs once when you press reset or power the board
void setup() {
  // initialize digital pin LED_BUILTIN as an output.
  pinMode(LED_PORT, OUTPUT);
  if (DEBUG == 1) {
    Serial.begin(9600);
  }
}

// the loop function runs over and over again forever
void loop() {
  i++;
  if (i < 255) {
    led_val -= 1;
  }
  if (255 <= i && i < 510) {
    led_val = 0;
  }
  if (510 <= i && i < 765) {
    led_val += 1;
  }
  if (i >= 765) {
    i = 0;
  }

  // This code does not work as intended if LED_PORT is set to use Digital port.
  // Make sure LEDs are connected PWM capable ports: 3,5,6,9,10,11
  analogWrite(LED_PORT, led_val);

  if (DEBUG == 1) { // Checking output via serial port
    Serial.print(i);
    Serial.print("\t");
    Serial.print("LedVal: ");
    Serial.print(led_val);
    Serial.print("\n");
  }

  delay(WAIT);
}
