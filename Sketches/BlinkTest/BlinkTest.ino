/* 
 *  Simple blinking protocol written by Taylor Shin.
 *  July 19th 2022
 */

// LED ON/OFF time (delay in miliseconds)
#define ON_DELAY 333
#define OFF_DELAY 333

// LED Port (Digital Port 12, D12, for now. But you can define 1-13 anytime. 
// Arduino UNO uses 13 as its default built-in LED.
#define LED_PORT 13

// This bitstream data will be blinked. 
#define DATA B0010101

// Fast writing methods
// https://www.best-microcontroller-projects.com/arduino-digitalwrite.html
#define setPin(b) ( (b)<8 ? PORTD |=(1<<(b)) : PORTB |=(1<<(b-8)) )
#define clrPin(b) ( (b)<8 ? PORTD &=~(1<<(b)) : PORTB &=~(1<<(b-8)) )
#define tstPin(b) ( (b)<8 ? (PORTD &(1<<(b)))!=0 : (PORTB &(1<<(b-8)))!=0 )

byte data = DATA;

// the setup function runs once when you press reset or power the board
void setup() {
  // initialize digital pin LED_BUILTIN as an output.
  // pinMode(LED_BUILTIN, OUTPUT);
  pinMode(LED_PORT, OUTPUT);
}

void led_on() {
  // digitalWrite(LED_PORT, HIGH);   // turn the LED on (HIGH is the voltage level)
  setPin(LED_PORT);
  delay(ON_DELAY);                       // wait for designated time
}

void led_off() {
  // digitalWrite(LED_PORT, LOW);    // turn the LED off by making the voltage LOW
  clrPin(LED_PORT);
  delay(OFF_DELAY);                       // wait for designated time
}

// the loop function runs over and over again forever
void loop() {
  for (int i=0, mask=1; i<8; ++i, mask = mask<<1) {
    if (data & mask) {
      led_on(); 
    }
    else {
      led_off();
    }
  } // for (int i=0, mask=1; i<8; ++i, mask = mask<<1) {
}
