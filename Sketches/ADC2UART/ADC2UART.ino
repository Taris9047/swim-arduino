/*
 * A Simple ADC to UART signal converter. Now blinks the built in LED.
 *
 * The sketch is coded for RPI Pico. But it will be usable for Arduino as well!
 *
 */

/* Setting up LED and ADC ports */
/* 
 * We are using Builtin LED at the moment. But we'll have to use
 * a Blue LED with 3.3 V logic later.
 */
#define UART_OUTPUT_LED LED_BUILTIN
/* ADC Port. RPI Pico has 3 ADC Ports:
 * GP26, GP27, GP28.
 * We'll use GP26, or pin number 31 at RPI Pico.
 */
#define ADC_IN0 26
#define ADC_IN1 27
#define ADC_IN2 28

#define DEBUG 1
//#undef DEBUG

/* UART LED Signal sequence interval in ms */
#define UART_OUTPUT_INTERV 100
#define UART_OUTPUT_WAIT_INTERV UART_OUTPUT_INTERV*5

/* Analog Input storage */
static uint32_t analog_input_0, analog_input_1, analog_input_2;

/* Setting up UART packet space */
#define ADC_BITS 12
#define DATA_BITS 8
#define UART_PACKET_LEN 12
uint8_t uart_seq_H[UART_PACKET_LEN];
uint8_t uart_seq_L[UART_PACKET_LEN];

/* Encoding given adc value to 8 bit UART */
/* We'll be using 8 data bits, and 1 parity bit and 2 stop bits */
uint8_t UART_Encode(uint32_t adc_val) {
  uint8_t adc_val_H = (uint8_t)( (adc_val & 0b000000000000111100000000)>>8 );
  uint8_t adc_val_L = (uint8_t)( (adc_val & 0b000000000000000011111111) );
  uint8_t parity, n_ones;
  uint8_t stop_bit = 1;
  uint8_t start_bit = 0;

  uint8_t i = 0;
  uart_seq_H[0] = start_bit;
  n_ones = 0;
  for (i=0; i<DATA_BITS; ++i) {
    if (i<ADC_BITS-DATA_BITS) uart_seq_H[UART_PACKET_LEN-4-i] = ( (adc_val_H>>i) & 0b00000001 );
    else uart_seq_H[UART_PACKET_LEN-4-i] = 0;
    if (uart_seq_H[UART_PACKET_LEN-4-i]) n_ones++;
  }
  parity = n_ones%2;
  uart_seq_H[UART_PACKET_LEN-3] = parity;
  uart_seq_H[UART_PACKET_LEN-2] = stop_bit;
  uart_seq_H[UART_PACKET_LEN-1] = stop_bit;

  uart_seq_L[0] = start_bit;
  n_ones = 0;
  for (i=0; i<DATA_BITS; ++i) {
    uart_seq_L[UART_PACKET_LEN-4-i] = ( (adc_val_L>>i) & 0b00000001 );
    if (uart_seq_L[UART_PACKET_LEN-4-i]) n_ones++;
  }
  parity = n_ones%2;
  uart_seq_L[UART_PACKET_LEN-3] = parity;
  uart_seq_L[UART_PACKET_LEN-2] = stop_bit;
  uart_seq_L[UART_PACKET_LEN-1] = stop_bit;

  return 0;
}

/* Sending UART Signal */
void SendUART() {
  uint8_t i = 0;
  digitalWrite(UART_OUTPUT_LED, HIGH);
  delay(UART_OUTPUT_WAIT_INTERV);
  for (i=0; i<UART_PACKET_LEN; i++) {
    if (uart_seq_H[i]) {
      digitalWrite(UART_OUTPUT_LED, HIGH);
      delay(UART_OUTPUT_INTERV);
    }
    else {
      digitalWrite(UART_OUTPUT_LED, LOW);
      delay(UART_OUTPUT_INTERV);
    }
  }
  digitalWrite(UART_OUTPUT_LED, HIGH);
  delay(UART_OUTPUT_WAIT_INTERV);
  for (i=0; i<UART_PACKET_LEN; i++) {
    if (uart_seq_L[i]) {
      digitalWrite(UART_OUTPUT_LED, HIGH);
      delay(UART_OUTPUT_INTERV);
    }
    else {
      digitalWrite(UART_OUTPUT_LED, LOW);
      delay(UART_OUTPUT_INTERV);
    }
  }
  digitalWrite(UART_OUTPUT_LED, HIGH);
}

void setup() {
  /* Setting up LED output */
  pinMode(UART_OUTPUT_LED, OUTPUT);
  digitalWrite(UART_OUTPUT_LED, HIGH);
  /* For Debug */
#ifdef DEBUG
  Serial.begin(115200);
#endif

}

void loop() {

  int adc_in = analogRead(ADC_IN0);
  /* RPI Pico has 12 bit ADC */
  analog_input_0 = map(adc_in, 0, 4095, 0, (1<<12));

#ifdef DEBUG
  Serial.print("ADC_IN_0:");
  Serial.print(analog_input_0);
  Serial.print("\n");
  delay(100);
#endif

  UART_Encode(analog_input_0);
#ifdef DEBUG
  Serial.println("Sending...");
  Serial.println("Upper part");
  for (int i=0; i<UART_PACKET_LEN; ++i) {
    Serial.print(uart_seq_H[i]);
    if (i == 0 || i == DATA_BITS || i == DATA_BITS+1) Serial.print(' ');
  }
  Serial.println("");
#endif
#ifdef DEBUG
  Serial.println("Lower part");
  for (int i=0; i<UART_PACKET_LEN; ++i) {
    Serial.print(uart_seq_L[i]);
    if (i == 0 || i == DATA_BITS || i == DATA_BITS+1) Serial.print(' ');
  }
  Serial.println("");
#endif
  SendUART();

#ifdef DEBUG
  Serial.println("");
#endif

}
