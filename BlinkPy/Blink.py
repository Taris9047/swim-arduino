#!/usr/bin/env -m python
import pyfirmata
import signal
import sys
import itertools

#
# Simple LED blinking example for PyFirmata
#
# Written by Taylor Shin
# July 22th 2022
#

# Ports
LED = 13

# Blink Data
data = [ 0, 0, 1, 0, 0, 1, 1 ]
data_iter = itertools.cycle(data)

# Delay in seconds
delay_on = 0.333
delay_off = 0.250

# Arduino Port (Differs by machine setting and OS)
# On Windows, it's just a simple COM + number. But on Linux, 
# it could be /dev/ttyACM + number etc.
#
# You can find out which port the Arduino is connected to the machine
# from Arduino IDE's status bar.
#
ArduinoPort = 'COM3'

# Keyboard Interrupt signal handler - This Python program will run
# indefinitely. So, you'll need to use interrupt signal (CTRL+C)
# to break out from it.
#
def interrupt_handler(sig, frame):
    print("Interrupt detected! Resetting Arduino and exiting!!")
    # Making sure the LED we've working with is turned off before quitting.
    board.digital[LED].write(0)
    sys.exit()

print("Accessing to Arduino at {}".format(ArduinoPort))
board = pyfirmata.Arduino('COM3', baudrate=57600, timeout=5)

# This is optional...
#
# If you're using a certain pin frequently... 
# setting up a variable for that pin is useful.
#
# The pin code is 3 parts, divided by (:)s.
#
# pin type: pin number: in/out/pwm
#
# pin type: a or d (analog or digital)
# pin number: the pin number after A or D (see your Ardino board)
# in/out/pwm: i/o/p defines pin behavior as input/output/PWM.
#    Note that not all pins support PWM.
#
# i.e. We are designing Digital pin 13 as output to pin_LED by...
pin_LED = board.get_pin('d:13:o')


# Starting the main loop
print("Now bliking Arduino's Digital port {} with " 
    "{}(ON), {}(OFF) sec of interval".format(LED, delay_on, delay_off))
print("Hit Ctrl+C (Signal Interrupt) to exit.")

while True:
    if next(data_iter) == 1:
        # board.digital[LED].write(1)
        pin_LED.write(1)
        board.pass_time(delay_on)
    else:
        board.digital[LED].write(0)
        board.pass_time(delay_off)
    # Handling exit (Ctrl+C) Python way
    signal.signal(signal.SIGINT, interrupt_handler)
