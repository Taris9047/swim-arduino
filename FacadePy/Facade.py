#!/usr/bin/env -m python

#
# Facade.py
#
# LED facade cycling with PyFirmata
# Written by Taylor Shin
# July 22th 2022
#
import pyfirmata
import signal
import sys

# Ports -- Note that we will utilize PWM available ports.
# Arduino UNO has PWM available pins: 3, 5, 6, 9, 10, 11 
# In this example, we'll use pin 9
LED = 9

# Delay in seconds - Facade will need much shorter period so that it may 
# look more 'natural.' Using 50 ms of intervals.
delay = 0.05

# Arduino Port (Differs by machine setting and OS)
# On Windows, it's just a simple COM + number. But on Linux, 
# it could be /dev/ttyACM + number etc.
#
# You can find out which port the Arduino is connected to the machine
# from Arduino IDE's status bar.
#
ArduinoPort = 'COM3'

# Keyboard Interrupt signal handler - This Python program will run
# indefinitely. So, you'll need to use interrupt signal (CTRL+C)
# to break out from it.
#
def interrupt_handler(sig, frame):
    print("Interrupt detected! Resetting Arduino and exiting!!")
    # Making sure the LED we've working with is turned off before quitting.
    board.digital[LED].write(0)
    sys.exit()

print("Accessing to Arduino at {}".format(ArduinoPort))
board = pyfirmata.Arduino('COM3', baudrate=57600, timeout=5)

# This is optional...
#
# Let's set up LED pin (pin 9) as PWM
pin_LED = board.digital[LED]
pin_LED.mode = pyfirmata.PWM

# Starting the main loop
print("Now bliking Arduino's Digital port {} with " 
    "{}(ON), {}(OFF) sec of interval".format(LED, delay, delay))
print("Hit Ctrl+C (Signal Interrupt) to exit.")

i = 0
LED_brightness = 1.0 # Starting with maximum (100 %) brightness
while True:

    if i < 100:
        pin_LED.write(LED_brightness)
        LED_brightness -= 0.01

    elif i >= 100 and i < 200:
        LED_brightness = 0.0
        pin_LED.write(LED_brightness)

    elif i >= 200 and i < 300:
        pin_LED.write(LED_brightness)
        LED_brightness += 0.01

    else:
        i = 0

    print("{} Current LED Brightness: {:2f}".format(i, LED_brightness))
    i += 1
    board.pass_time(delay)

    # Handling exit (Ctrl+C) Python way
    signal.signal(signal.SIGINT, interrupt_handler)
